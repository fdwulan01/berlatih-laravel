<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi Sanberbook</title>
</head>
<body>
<h1> Buat Account Baru! </h1>
       <h3> Sign Up Form </h3>
       <form action="/welcome" method="POST">
        @csrf
           <label> First name : </label><br>
           <input type="text" name="nama_dpn"><br><br>

           <label> Last name : </label><br>
           <input type="text" name="nama_blkg"><br><br>

           <label> Gender : </label><br>
           <input type="radio" name="gender" value="Male"> Male <br>
           <input type="radio" name="gender" value="Female"> Female <br>
           <input type="radio" name="gender" value="Other"> Other <br><br>

           <label> Nationality : </label><br>
           <select name="Nationality">
                <option value=Indonesian> Indonesian </option>
                <option value=Malaysian> Malaysian </option>
                <option value=Singaporean> Singaporean </option>
                <option value=Others> Others </option>
           </select><br><br>

           <label> Language Spoken : </label><br>
           <input type="checkbox" name="language" value="Bahasa Indonesia"> Bahasa Indonesia <br>
           <input type="checkbox" name="language" value="English"> English <br>
           <input type="checkbox" name="language" value="Other"> Other <br>
           
           <label> Bio : </label><br>
           <textarea name="bio" cols="30" rows="10"></textarea><br><br>

           <input type="submit" value="Sign Up">
       </form>
</body>
</html>