<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function greet(Request $request){
        //dd($request->all());
        $nama_dpn = $request['nama_dpn'];
        $nama_blkg = $request['nama_blkg'];
        //echo "SELAMAT DATANG $nama";
        return view('greet', compact('nama_dpn','nama_blkg'));
}
}
?>
